#! /bin/bash
music_dir='/d/fftool/music'
[ ! -d /d/fftool/music ] && mkdir $music_dir
for f in "$@"; do
    filename=$(basename -- "$f") # get the filename without the full path
    echo "$filename"
    filename="$(echo $filename | tr -d '()' | sed s/' Official'// | sed s/' Music'// | sed s/' Video'//"
    # mv "$f" "$filename"
    # extension="${filename#*.}"
    filename="${filename%.*}" # the filename without .extension
    ffmpeg -i "${f}"  "${music_dir}/${filename}.mp3" 
done