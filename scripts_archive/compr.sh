#! /bin/bash

let i=0
mkdir 'comprfiles'

for f in "$@"


do
    filename="$(echo $f | tr -d '-' | tr -s ' ' '_' | tr '&' 'n' | tr -s '__' '_')"
    comprfile="compressed_$filename"
    mv "$f" "$filename"

    ffmpeg -i "$filename" "$comprfile"
    # ffmpeg -i "$filename" "compressed_$filename"
    ((i++))
    echo -e "\e[1A\e[K$i"

    # restore the name of the compressed file
    mv "$comprfile" "$filename"
    # move the compressed file to the comprfiles directorie
    mv "$filename" "./comprfiles/"
done

echo "$i files compressed"
