#! /bin/bash


# CHECK_MARK="\033[0;32m\xE2\x9C\x94\033[0m"
CHECK_MARK="\033[0;32m\xE2\x9C\x93\033[0m"

# 0xE2 0x9C 0x93 <-- the difference is here

echo -e "\n\e[36mDoing Things\e[0m\n"
let i=0
for n in {1..5}
do
    ((i++))
    echo -n "doing thing $i..."
    sleep 0.7
    echo -e "\\r${CHECK_MARK} thing $i done"

done
echo -e "\033[1;36m36m for this colored sentense\n"
echo -e "\033[1;32m40m for this colored sentense\n"
echo -e "\U0001F923"

# https://iterm2colorschemes.com/

# Recursively list all directories that contain one or more jpg image files

# """ ~% FILE="example.tar.gz"

# ~% echo "${FILE%%.*}"
# example

# ~% echo "${FILE%.*}"
# example.tar

# ~% echo "${FILE#*.}"
# tar.gz

# ~% echo "${FILE##*.}"
# gz"""
