#! /bin/bash

FZF_DEFAULT_OPT='--ansi --color=dark --reverse --border --height=40%'

menu(){
    echo -e "\033[36m1: convert MP4 to MP3\e[0m"
    echo -e "\033[36m2: record the screen\e[0m"
    echo -e "\033[36m3: extract\e[0m"
    echo -e "\033[36m4: others\e[0m"   
}

backup_n_extraction(){
    adb backup -f "${target_dir}/${backup_name}.ab" -noapk -noobb -noshared -all -system -keyvalue &&
    java.exe -jar abe.jar unpack "${target_dir}/${backup_name}.ab" "${target_dir}/${backup_name}.tar" $password &&
    # --strip-components 3 <- to extract the file from subdirectories
    tar -xf "${target_dir}/${backup_name}.tar"  -C  "$target_dir"  --wildcards --no-anchored \
        'com.android.providers.settings.data' --strip-components 3
}

extract(){
    echo "name of the backup"
    read -p "> " target_name
    echo "PASSWORD you'll use on the phone"
    read -p "> " password
    extract_dir="/d/fftool/extract"
    target_dir="/d/fftool/extract/$target_name"
    d=$(date +%d%m%Y)
    backup_name="${target_name}_${d}_backup"

    [ ! -d $extract_dir ] && mkdir $extract_dir && cd $extract_dir || cd $extract_dir
    [ ! -d $target_dir ] && mkdir $target_dir && backup_n_extraction || backup_n_extraction
}        

conversion_with_cut(){
    echo -e "\033[36moptions:"
    echo -e "\033[36m1: Beginning to X"
    echo -e "\033[36m2: X to Y"
    echo -e "\033[36m2: Y to END"
    read -p '> ' option

    for f in "$@"; do
    filename=(basename -- "$f") # get the filename without the full path
    # filename="$(echo $filename | tr -d '-' | tr -s ' ' '_' | tr '&' 'n' | tr -s '__' '_')"
    # mv "$f" "$filename"
    extension="${filename#*.}"
    filename="${filename%.*}" # the filename without .extension
    ffmpeg -i "$f" -ss "$t_beginning" -t "$t_end" "$filename.mp3" 
    done
}

convert_vid_mp3(){
    music_dir='/d/fftool/music'
    [ ! -d /d/fftool/music ] && mkdir $music_dir
    read -p 'drag and drop the file here:' f
    echo -e "\033[36moptions:"
    echo -e "\033[36m1: with cut"
    echo -e "\033[36m2: without cut"
    read -p '> ' option
    echo $f
    [[ "$option" == '1' ]] && conversion_with_cut || conversion.sh "$f"
}

screen_recorder(){
    records_dir="/d/fftool/records"
    d=$(date +%d%m%Y_%H%M%S)
    [[ ! -d "$records_dir" ]] && mkdir "$records_dir" && cd "$records_dir" || cd "$records_dir"
    formats=(MKV MP4)
    format_choice=$(printf "%s\n" "${formats[@]}" | fzf $FZF_DEFAULT_OPT --prompt='choose a format: ')
    [[ $format_choice == 'MP4' ]] && format='mp4' || format='mkv' # set the format of the output
    
    # ffmpeg -f dshow -rtbufsize 2147.48M -framerate 25 -pixel_format bgr0 \
    #     -i audio="virtual-audio-capturer":video="screen-capture-recorder" -c:v libx264 \
    #     -r 30 -preset ultrafast -tune zerolatency -crf 28 -pix_fmt yuv420p \
    #     -movflags +faststart -c:a aac -ac 2 -b:a 128k -y "VID_${d}.$format"
}

main(){
    fftool_dir="/d/fftool"
    # create the fftool dir only if it doesn't exist
    [ ! -d "$fftool_dir" ] && mkdir "$fftool_dir"
    # clasic prompt
    # menu
    # read -p "> " 
    
    # using fuzzy finder
    choice=$(menu | fzf $FZF_DEFAULT_OPT --prompt='choice a number: ')
    choice="${choice%%:*}"


    case "$choice" in
        1) convert_vid_mp3 ;;
        2) screen_recorder ;;
        3) extract ;;
        *) echo -e "\033[31mcommand not foumd." ;;
    esac

}

main