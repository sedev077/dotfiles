#! /bin/bash

# read -p "Format of the input file:  " i_format
# read -p "Format of the output file:  " o_format

for f in "$@"
do

# filename="$(echo $f | mv "$f" "${f/"mp4"/" "}")

# echo "OUTPUT: $filename$o_format"
# echo "input: $filename$i_format"
# echo "output: $filename$o_format"

# ffmpeg -i "$f.$i_format" "./$f.$o_format"
ffmpeg -i "$f" "./$f.mp3"

done