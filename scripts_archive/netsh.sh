#! /bin/bash

menu(){
    echo -e "\n\e[36m\tMenu"
    echo -e "\033[36m1: setup a new Mobile Hotspot\e[0m"
    echo -e "\033[36m2: Manage the Mobile Hotspot\e[0m"
}

setup_hotspot(){
    read -p "ssid: " ssid
    read -p "password: " password
    NETSH WLAN set hostednetwork mode=allow ssid="${ssid}" key="${password}"
}

manage_hotspot(){
    read -p "start or stop? " choice
    NETSH WLAN "${choice}" hostednetwork
}

main(){
    menu
    read -p '> ' choice
    [ ! "$choice" = 1  ] && manage_hotspot || setup_hotspot
}

main