#! /bin/bash

let i=0

for f in "$@"
do
    filename="$(echo $f | tr -d '-' | tr -s ' ' '_' | tr '&' 'n' | tr -s '__' '_')"
    mv "$f" "$filename"

    ffmpeg -i "$filename" "compressed_$filename"
    # ffmpeg -i "$filename" "compressed_$filename"
    ((i++))
    echo -e "\e[1A\e[K$i"
    # mv "compressed_$filename" "$filename"
done

echo "$i files compressed"
