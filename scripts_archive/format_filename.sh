#! /bin/bash

let i=0

for f in "$@"
do
    filename="$(echo $f | tr -s '-' '_' | tr -s ' ' '_' | tr -s '__' '_' | tr '&' 'n')"
    mv "$f" "$filename"
    ((i++))
    echo -e "\e[1A\e[Kfilename formated: $i"
done