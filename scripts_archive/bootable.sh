#! /bin/bash

bootable_usb(){
    diskpart && list disk && read -p '> ' disk &&
    select disk "${disk}" && clean && create partition primary &&
    select partition 1 && format fs=ntfs quick && active && exit
}

bootable_usb