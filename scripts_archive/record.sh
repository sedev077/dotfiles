#! /bin/bash

read -p 'output name: ' name
read -p 'output format: ' format

mkdir /d/records/
cd /d/records

ffmpeg -f dshow -rtbufsize 2147.48M -framerate 25 -pixel_format bgr0 \
-i audio="virtual-audio-capturer":video="screen-capture-recorder" -c:v libx264 \
-r 30 -preset ultrafast -tune zerolatency -crf 28 -pix_fmt yuv420p \
-movflags +faststart -c:a aac -ac 2 -b:a 128k -y "$name.$format"

read -p 'Do you want to play the last recorded movie? ' choice
if [[ $choice == 'y' ]]; then
    vlc "$name.$format"
fi

echo "cd d/records/"