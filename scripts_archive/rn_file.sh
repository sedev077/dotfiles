#! /bin/bash

read -p "Chars to replace " chars1
read -p "replaced by " chars2
let i=0
echo renaming...
for f in "$@"
do
    mv "$f" "${f/$chars1/$chars2}"
    ((i++))
    echo -e "\e[1A\e[K$i"
done

echo "$i files renamed"

