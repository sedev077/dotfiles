#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"
[ -d "$HOME/.cargo/bin" ] && PATH="$HOME/.cargo/bin:$PATH"

[ $(command -v helix) ] && EDITOR=helix || EDITOR=vim

source $HOME/aliasrc
source $HOME/functionrc

# GREEN=$(tput setaf 41)
# ORANGE=$(tput setaf 166)
# BLUE=$(tput setaf 43)
# RESET=$(tput sgr0)
# 
# user="${GREEN}\u${RESET}"
# hostname="${ORANGE}\h${RESET}"
# working_dir="${BLUE}\W${RESET}"

# PS1="[${user}@${hostname} ${working_dir}]\$ "
PS1="[\u@\h \W]\$ "


eval "$(starship init bash)"
. "$HOME/.cargo/env"
